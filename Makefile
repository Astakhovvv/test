NAME:=test
DOCKER_REPOSITORY:=registry.gitlab.com/astakhovvv
DOCKER_IMAGE_NAME:=$(DOCKER_REPOSITORY)/$(NAME)
VERSION:=latest
VERSION4TEST:=4test


build-container:
	docker build -t $(DOCKER_IMAGE_NAME):$(VERSION4TEST) .
	docker push $(DOCKER_IMAGE_NAME):$(VERSION4TEST)


test-container:
	docker run -dp 9898:9898 --name=test $(DOCKER_IMAGE_NAME):$(VERSION4TEST)
	docker ps
	sleep 20
	curl docker:9898

push-container:
	docker pull $(DOCKER_IMAGE_NAME):$(VERSION4TEST)
	docker tag $(DOCKER_IMAGE_NAME):$(VERSION4TEST) $(DOCKER_IMAGE_NAME):$(VERSION)
	docker push $(DOCKER_IMAGE_NAME):$(VERSION)
